#!/bin/bash

set -e
set -x

cd lfortran
wget https://www.dropbox.com/s/plqe7dfzxnce3hi/lfortran_dist-0.1.5-1-linux-64.tar.gz -O lfortran_dist.tar.gz
tar xzf lfortran_dist.tar.gz
rm lfortran_dist.tar.gz
cd ..

curl -Lo warp-packer https://github.com/dgiagio/warp/releases/download/v0.3.0/linux-x64.warp-packer
chmod +x warp-packer

./warp-packer --arch linux-x64 --input_dir lfortran --exec launch --output lfortran.bin
chmod +x lfortran.bin
